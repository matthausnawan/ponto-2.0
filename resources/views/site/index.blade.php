@extends('template.site')


@section('principal')

<!-- para trocar a cor do background do banner =['bg-primary','bg-success','bg-warning'] -->
<div class="header bg-success pt-5 pb-7">
      <div class="container">
        <div class="header-body">
          <div class="row align-items-center">
            <div class="col-lg-6">
              <div class="pr-5">
                <h1 class="display-2 text-white font-weight-bold mb-0">Não colecione coisas, colecione momentos</h1>
                <h2 class="display-4 text-white font-weight-light"> Estamos em manuntenção  de forma planejada, estruturada e transparente.
            </p></h2>
                <p class="text-white mt-4"></p>
                <div class="mt-5">
                  <a href="{{route('register') }}" class="btn btn-neutral my-2">Cadastre seu ponto</a>
                  <a href="{{ route('pontos') }}" class="btn btn-default my-2">Colecte algo</a>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="row pt-5">
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-body">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow mb-4">
                        <i class="ni ni-active-40"></i>
                      </div>
                      <h5 class="h3">Minimalismo</h5>
                      <p>Argon comes with over 70 handcrafted components.</p>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-body">
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow mb-4">
                        <i class="ni ni-active-40"></i>
                      </div>
                      <h5 class="h3">Empreededorismo Social</h5>
                      <p>Fully integrated and extendable third-party plugins that you will love.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 pt-lg-5 pt-4">
                  <div class="card mb-4">
                    <div class="card-body">
                      <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow mb-4">
                        <i class="ni ni-active-40"></i>
                      </div>
                      <h5 class="h3">Economia Circular</h5>
                      <p>From simple to complex, you get a beautiful set of 15+ page examples.</p>
                    </div>
                  </div>
                  <div class="card mb-4">
                    <div class="card-body">
                      <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow mb-4">
                        <i class="ni ni-active-40"></i>
                      </div>
                      <h5 class="h3">Design Sustentável</h5>
                      <p>You will love how easy is to to work with Argon.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>

@stop
