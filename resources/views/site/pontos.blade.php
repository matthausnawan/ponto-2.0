@extends('template.site')

@section('principal')
<section class="py-6 pb-9 bg-default">
      <div class="row justify-content-center text-center">
        <div class="col-md-12">
          <h2 class="display-3 text-white">Não colecione coisa, colecione momentos</h2>
            <p class="lead text-white">
                Estamos em manuntenção  de forma planejada, estruturada e transparente.
            </p>
        </div>
      </div>
      <div class="row justify-content-center">
            <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative input-group-merge">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input class="form-control" placeholder="Pequise por ex: eletronicos,livros" type="text">
                    </div>
                </div>
                    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
            </form>
      </div>
    </section>
<section class="section section-lg pt-lg-0 mt--7">
    <div class="container">
        @foreach($data->chunk(3) as $chunk)
            <div class="card-deck mt-5 d-flex">
                @foreach($chunk as $p)
                    <div class="card card-lift--hover shadow border-0">
                        <img class="card-img-top" src="{{ Storage::url($p->ponto_logo)}}" alt="Imagem de capa do card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $p->ponto_nome}}</h5>
                            <p class="card-text">{{$p->ponto_sobre}}</p>
                            <p class="card-text"><small class="text-muted">cadastrado por: {{ $p->user->apelido}}</small></p>
                        </div>
                        <div class="card-footer text-center pull-right">
                            <a href="" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Visualizar"><i class="fas fa-eye"></i></a>
                            <a href="" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Favoritos"><i class="fas fa-heart"></i></a>
                            <a href="" class="btn btn-info btn-sm" data-toggle="tooltip" title="Compartilhar"><i class="fas fa-share"></i></a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
</section>
@endsection
