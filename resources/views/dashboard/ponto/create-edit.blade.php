<form action="{{ route('ponto.store') }}" method="POST" enctype="multipart/form-data">
    <div class="card-body">
        @csrf
            <h6 class="heading-small text-muted mb-4">User information</h6>
            <div class="pl-lg-4">
            <div class="row">
                <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-control-label" for="input-username">Qual nome do seu Ponto?</label>
                    <input type="text" id="input-username" name="ponto_nome" class="form-control" placeholder="Use um bom nome">
                </div>
                </div>
                <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label" for="input-email">Responsável pelo ponto</label>
                    <input type="text" id="input-email" name="ponto_responsavel" class="form-control" placeholder="">
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label" for="input-first-name">Telefone</label>
                    <input type="text" id="telefone" name="ponto_telefone" class="form-control" placeholder="(0DD)0000-0000">
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label" for="input-last-name">Email</label>
                    <input type="email" id="input-last-name" name="ponto_email" class="form-control" placeholder="email@email.com" >
                </div>
                </div>
            </div>
            </div>
            <hr class="my-4" />
            <!-- Address -->
            <h6 class="heading-small text-muted mb-4">Endereço e Localização</h6>
            <div class="pl-lg-4">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="form-control-label" for="input-country">Cep</label>
                        <input type="text" name="ponto_cep" id="cep" class="form-control" placeholder="CEP">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <label class="form-control-label" for="input-address">Rua/Logradouro</label>
                        <input id="rua" name="ponto_rua" class="form-control" placeholder="endereço completo + numero" value="" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label" for="input-city">Cidade</label>
                    <input type="text" name="ponto_cidade" id="cidade" class="form-control" placeholder="Cidade" value="">
                </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label" for="input-country">Bairro</label>
                    <input type="text" name="ponto_bairro" id="bairro" class="form-control" placeholder="Bairro" value="">
                </div>
                </div>

            </div>
            </div>
            <hr class="my-4" />
            <!-- Description -->
            <h6 class="heading-small text-muted mb-4">Sobre</h6>
            <div class="pl-lg-4">
            <div class="form-group">
                <label class="form-control-label">Sobre</label>
                <textarea rows="4" name="ponto_sobre" class="form-control" placeholder="A few words about you ...">Fale um pouco do seu ponto</textarea>
            </div>
                <div class="form-group">
                    <label for="form-control-label">Upload Logo</label>
                    <input type="file" name="ponto_logo" class="form-control">
                </div>
            </div>
    </div>
    <!-- card-body -->

    <div class="card-footer text-center">
        <button type="submit" class="btn btn-success">Salvar Dados</button>
    </div>

</form>

@section('js')
<script src="{{ asset('js/viaCep.js') }}"></script>
<script src="{{ asset('js/jquery.mask.min.js') }}"></script>

<script>
        $(document).ready(function(){
            $('#telefone').mask('(000)0000-0000');
            $('#cep').mask('00.000-000');
        })
</script>

@stop
