@extends('template.site')


@section('principal')
<div class="row mt-5">
    <div class="container">
    <div class="card col-md-8 offset-2">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Cadastro de Ponto</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{route('site')}}" class="btn btn-sm btn-primary">Voltar pro site</a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="{{ route('register') }}" method="POST">
                  @csrf
                <h6 class="heading-small text-muted mb-4">Informações do Usuário</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Nome Completo</label>
                        <input type="text" id="input-username" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Digite seu nome" name="name" value="{{ old('name')}}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Endereço de e-mail</label>
                        <input type="email" id="input-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="exemplo@sexample.com" name="email" value="{{ old('email')}}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Como gostaria de ser chamado(a)?</label>
                        <input type="text" id="input-first-name" class="form-control" placeholder="Digite um nome" name="apelido">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Telefone para contato</label>
                        <input type="text" id="input-last-name" class="form-control" placeholder="Sobre Nome" name="telefone">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- <hr class="my-4" /> -->
                <!-- Address -->
                <!-- <h6 class="heading-small text-muted mb-4">Endereço e Localização</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="cep">CEP</label>
                        <input id="cep" class="form-control" placeholder="apenas numeros" value="" type="text" name="cep">
                      </div>
                    </div>
                    <div class="col-md-8">
                      <div class="form-group">
                        <label class="form-control-label" for="rua">Endereço completo</label>
                        <input id="rua" class="form-control" placeholder="Rua, numero" value="" type="text" name="rua">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="numero">Numero</label>
                        <input id="numero" class="form-control" placeholder="Rua, numero" name="numero" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="cidade">Cidade</label>
                        <input type="text" id="cidade" class="form-control" placeholder="Cidade" name="cidade">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="bairro">Bairro</label>
                        <input type="text" id="bairro" class="form-control" placeholder="Bairro" name="bairro">
                      </div>
                    </div>
                  </div>
                </div> -->
                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Credenciais</h6>
                <div class="pl-lg-4">
                    <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Senha</label>
                        <input type="password" id="input-country" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Digite uma senha" name="password" value="{{ old('password')}}">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Repetir Senha</label>
                        <input type="password" id="input-country" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Repita a senha" name="password_confirmation" value="{{ old('password_confirmation')}}">
                        @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    </div>
                </div>

                <hr class="my-4">
                <div class="button text-center">
                    <button type="submit" class="btn btn-success">Cadastrar</button>
                </div>
              </form>
            </div>
          </div>
    </div>

</div>


@stop
