<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePontosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pontos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('ponto_nome')->nullable();
            $table->string('ponto_responsavel')->nullable();
            $table->string('ponto_telefone')->nullable();
            $table->string('ponto_email')->nullable();
            $table->string('ponto_cep')->nullable();
            $table->string('ponto_rua')->nullable();
            $table->string('ponto_bairro')->nullable();
            $table->string('ponto_cidade')->nullable();
            $table->text('ponto_sobre')->nullable();
            $table->string('ponto_logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pontos');
    }
}
