<?php


Route::view('/','site.index')->name('site');
Route::get('pontos_de_coleta','PontoController@lista')->name('pontos');

Auth::routes();

Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
    Route::view('/','dashboard.index')->name('dashboard.index');

    Route::resource('ponto','PontoController');
});
