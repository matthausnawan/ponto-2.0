<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ponto;

class PontoController extends Controller
{
    public function store(Request $request){

        $user = auth()->user();
        $data = $request->all();

        if($request->hasFile('ponto_logo')){
            $data['ponto_logo'] = $request->ponto_logo->store('pontos');
        }

        $user->ponto()->create($data);

        return redirect()->route('dashboard.index');
    }

    public function lista(){

        $data = Ponto::all();
        return view('site.pontos',compact('data'));

    }
}
