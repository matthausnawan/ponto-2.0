<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ponto extends Model
{
    protected $fillable = [
        'ponto_nome',
        'ponto_responsavel',
        'ponto_telefone',
        'ponto_email',
        'ponto_cep',
        'ponto_rua',
        'ponto_cidade',
        'ponto_bairro',
        'ponto_sobre',
        'ponto_logo'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
